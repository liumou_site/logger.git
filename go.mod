module gitee.com/liumou_site/logger

go 1.19

require (
	github.com/mattn/go-colorable v0.1.13
	github.com/spf13/cast v1.5.0
)

require (
	github.com/mattn/go-isatty v0.0.16 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
)
